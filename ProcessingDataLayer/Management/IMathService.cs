﻿using System;
using System.Collections.Generic;

namespace ProcessingDataLayer
{
	public interface IMathService
	{
		IList<int> FindEvenNumbers(IList<int> numbers);
		KeyValuePair<IList<int>, int> BubbleSort(IList<int> listBoxItems);
		IList<DateTime> GetDaysBetweenDates(DayOfWeek day, DateTime from, DateTime to);
		IList<int> GetFibonacciSerie(int inputNumber);
		double CalculateDistance(double ax, double ay, double bx, double by);
		string GenerateHash(string message);
		IList<Tuple<int, int>> GetNumbersFromMatrix(float[][] matrix, float number);
	}
}