﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcessingDataLayer
{
	public class MathService : IMathService
	{
		public KeyValuePair<IList<int>, int> BubbleSort(IList<int> list)
		{
			var items = list.ToList();
			var exchanges = 0;
			for (int i = 0; i < items.Count; i++)
			{
				for (int j = i + 1; j < items.Count; j++)
				{
					if (items[j] > items[i])
					{
						var aux = items[i];
						items[i] = items[j];
						items[j] = aux;

						exchanges++;
					}
				}
			}

			return new KeyValuePair<IList<int>, int>(items, exchanges);
		}

		public double CalculateDistance(double ax, double ay, double bx, double by)
		{
			return Math.Sqrt(Math.Pow(ax - bx, 2) + Math.Pow(ay - by, 2));
		}

		public IList<int> FindEvenNumbers(IList<int> numbers)
		{
			return numbers.Where(x => x % 2 == 0).ToList();
		}

		/// <summary>
		/// Generate HASH
		/// Procedure:
		///		We generate the hashing using the module concept.
		///		Transforming the message into byte array and then get the module.
		///		If the message is empty (or almost) the hash result would be 
		///		a small value. To Avoid that we multiply the value with a constat (cripto)
		///		To resolve the multiplication we use and external method that resolve it.
		///		After that the module is calculated based on the new array.
		///		The Result is the custom has vale
		/// </summary>
		/// <param name="message"></param>
		/// <returns></returns>
		public string GenerateHash(string message)
		{
			byte[] array = Encoding.ASCII.GetBytes(message);
			byte[] cripto = ("FF213A83ABC27929123AFFBC").ToCharArray()
				.Select(x => Convert.ToByte(x)).ToArray();
			
			var result = Multiply(array, cripto);
			var intMod = GenerateMod(result, 0xFFFFFFFFFFFFF);
			return Convert.ToString(intMod, 16).ToUpper();
		}

		public IList<DateTime> GetDaysBetweenDates(DayOfWeek day, DateTime from, DateTime to)
		{
			
			if (from > to)
				throw new Exception("La fecha Desde debe ser menor a Hasta");

			var current = from;
			while (current.DayOfWeek != day)
			{
				current = current.AddDays(1);
			}

			var list = new List<DateTime>();

			if (current > to)
				return list;

			while (current <= to)
			{
				list.Add(current);
				current = current.AddDays(7);
			}

			return list;
		}

		public IList<int> GetFibonacciSerie(int size)
		{
			//1,1,2,3,5
			var result = new List<int>();

			if (size > 0)
				result.Add(1);
			else
				return result;

			if (size > 1)
				result.Add(1);
			else
				return result;

			for (int i = 2; i < size; i++)
			{
				result.Add(result[i-2] + result[i-1]);
			}

			return result;
		}

		/// <summary>
		/// This Method generates a module based on 
		/// the standar division algorithm.
		/// The main key is take numbers and divide it
		/// separately get the module and continue
		/// </summary>
		/// <param name="numberA"></param>
		/// <param name="numberB"></param>
		/// <returns></returns>
		public long GenerateMod(byte[] numberA, long numberB)
		{
			long result = 0;

			for (long i = 0; i < numberA.Length; i++)
			{
				result = result << 8;
				result += (numberA[i] & 0xFF);

				//calculate the module
				result %= numberB;
			}

			return result;
		}

		/// <summary>
		/// This code has been taken from:
		/// http://tekuconcept.blogspot.com/2013/06/performing-operations-on-byte-arrays.html
		/// The main idea is multiply byte to byte and keep the overflow flag to propagate at the end of the array
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public byte[] Multiply(byte[] a, byte[] b)
		{
			List<byte> ans = new List<byte>();

			byte ov, res;
			int idx = 0;
			for (int i = 0; i < a.Length; i++)
			{
				ov = 0;
				for (int j = 0; j < b.Length; j++)
				{
					short result = (short)(a[i] * b[j] + ov);

					// get overflow (high order byte)
					ov = (byte)(result >> 8);
					res = (byte)result;
					idx = i + j;

					// apply result to answer array
					if (idx < (ans.Count))
						ans = _add_(ans, res, idx);
					else ans.Add(res);
				}
				// apply remainder, if any
				if (ov > 0)
					if (idx + 1 < (ans.Count))
						ans = _add_(ans, ov, idx + 1);
					else ans.Add(ov);
			}

			return ans.ToArray();
		}

		/// <summary>
		/// helper class to add two numbers on the array
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="idx"></param>
		/// <param name="rem"></param>
		/// <returns></returns>
		private List<byte> _add_(List<byte> a, byte b, int idx = 0, byte rem = 0)
		{
			short sample = 0;
			if (idx < a.Count)
			{
				sample = (short)((short)a[idx] + (short)b);
				a[idx] = (byte)(sample % 256);
				rem = (byte)((sample - a[idx]) % 255);
				if (rem > 0)
					return _add_(a, (byte)rem, idx + 1);
			}
			else a.Add(b);

			return a;
		}

		public IList<Tuple<int, int>> GetNumbersFromMatrix(float[][] matrix, float number)
		{
			var result = new List<Tuple<int, int>>();

			for (int i = 0; i < matrix.Length; i++)
			{
				for (int j = 0; j < matrix[i].Length; j++)
				{
					if(matrix[i][j] == number)
						result.Add(new Tuple<int, int>(i, j));
				}
			}

			return result;
		}
	}
}
