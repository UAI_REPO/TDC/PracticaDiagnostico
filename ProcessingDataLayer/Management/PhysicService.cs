﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcessingDataLayer
{
	public class PhysicService : IPhysicService
	{
		public float CalculateVoltage(float resistance, float current)
		{
			return resistance * current;
		}

		public float CalculateCurrent(float resistance, float voltage)
		{
			return (voltage / resistance);
		}

		public float CalculateResistance(float current, float voltage)
		{
			return (voltage / current);
		}

		/// <summary>
		/// vox = vo * cos(angle)
		/// voy = vo * sin(angle)
		/// vy(t) = voy*t - 1/2 g * t^2 | v(t) = 0 | => t = vo/(1/2 * g)
		/// y(t) = voy * t + 1/2 g * t2
		/// x(t) = vox * t
		/// </summary>
		/// <param name="speed"></param>
		/// <param name="angle"></param>
		/// <returns></returns>
		public Tuple<double, double> CalculateParabolicShot(float speed, float angle)
		{
			double g = 9.81;
			double phy = angle * (Math.PI / 180);
			double vox = speed * Math.Cos(phy);
			double voy = speed * Math.Sin(phy);

			double t = voy / (0.5 * g);

			double x = vox * t;
			double y = voy * t * Math.Pow(0.5, 2);

			return new Tuple<double, double>(x, y);
		}
	}
}
