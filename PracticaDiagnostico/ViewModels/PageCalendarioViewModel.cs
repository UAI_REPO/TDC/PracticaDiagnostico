﻿using ProcessingDataLayer;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PracticaDiagnostico.ViewModels
{
	public class PageCalendarioViewModel : ViewModelBase
	{
    #region Fields
    DateTime from;
    DateTime to;
    IList<string> weekDays;
    string selectedDay;
    IList<DateTime> listDays;
    #endregion

    #region Properties
    public IMathService Service { get; set; }

    public DateTime From
    {
      get
      {
        return from;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref from, value);
        from = value;
      }
    }

    public DateTime To
    {
      get
      {
        return to;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref to, value);
        to = value;
      }
    }

    public IList<string> WeekDays
    {
      get
      {
        return weekDays;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref weekDays, value);
        weekDays = value;
      }
    }

    public string SelectedDay
    {
      get
      {
        return selectedDay;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref selectedDay, value);
        selectedDay = value;
      }
    }

    public IList<DateTime> ListDays
    {
      get
      {
        return listDays;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref listDays, value);
        listDays = value;
      }
    }

    #endregion

    #region Constructor
    public PageCalendarioViewModel()
    {
      Service = new MathService();
      From = new DateTime(2018, 01, 01);
      To = DateTime.Now;

      var culture = new System.Globalization.CultureInfo("es-AR");
      WeekDays = Enum.GetValues(typeof(DayOfWeek)).Cast<DayOfWeek>().Select(x => culture.DateTimeFormat.GetDayName(x)).ToList();

      SelectedDay = WeekDays[1];
    }
    #endregion

    #region Methods

    public void OnCalculateClick()
    {
      try
      {
        var index = WeekDays.IndexOf(SelectedDay);
        var day = Enum.GetValues(typeof(DayOfWeek)).Cast<DayOfWeek>().FirstOrDefault(x => x == (DayOfWeek)index);
        ListDays = Service.GetDaysBetweenDates(day, From, To);
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
    }
    #endregion
  }
}