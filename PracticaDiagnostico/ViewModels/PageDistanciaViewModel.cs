﻿using ProcessingDataLayer;
using ReactiveUI;
using System;

namespace PracticaDiagnostico.ViewModels
{
	public class PageDistanciaViewModel : ViewModelBase
	{
    #region Fields
    double distance;
    string coordinateAX;
    string coordinateAY;
    string coordinateBX;
    string coordinateBY;
    bool enableCalculateButton;
    #endregion

    #region Properties

    public bool EnableCalculateButton
    {
      get
      {
        return enableCalculateButton;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref enableCalculateButton, value);
        enableCalculateButton = value;
      }
    }

    public string CoordinateAX
    {
      get
      {
        return coordinateAX;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref coordinateAX, value);
        coordinateAX = value;
        ValidateCalculateButton();
      }
    }

    public string CoordinateAY
    {
      get
      {
        return coordinateAY;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref coordinateAY, value);
        coordinateAY = value;
        ValidateCalculateButton();
      }
    }

    public string CoordinateBX
    {
      get
      {
        return coordinateBX;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref coordinateBX, value);
        coordinateBX = value;
        ValidateCalculateButton();
      }
    }

    public string CoordinateBY
    {
      get
      {
        return coordinateBY;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref coordinateBY, value);
        coordinateBY = value;
        ValidateCalculateButton();
      }
    }

    public double Distance
    {
      get
      {
        return distance;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref distance, value);
        distance = value;
      }
    }

    public IMathService Service { get; set; }
    #endregion

    #region Constructor
    public PageDistanciaViewModel()
    {
      Service = new MathService();
    }
    #endregion

    #region Methods
    public void ValidateCalculateButton()
    {
      double value;

      EnableCalculateButton = 
        double.TryParse(CoordinateAX, out value) &&
        double.TryParse(CoordinateAY, out value) &&
        double.TryParse(CoordinateBX, out value) &&
        double.TryParse(CoordinateBY, out value);
    }

    public void OnCalculateClick()
    {
      try
      {
        double ax;
        double ay;
        double bx;
        double by;

        double.TryParse(CoordinateAX, out ax);
        double.TryParse(CoordinateAY, out ay);
        double.TryParse(CoordinateBX, out bx);
        double.TryParse(CoordinateBY, out by);

        Distance = Service.CalculateDistance(ax, ay, bx, by);
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
    }

    #endregion
  }
}