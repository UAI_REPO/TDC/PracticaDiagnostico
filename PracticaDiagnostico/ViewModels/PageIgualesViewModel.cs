﻿using Avalonia.Controls;
using Avalonia.Media.Imaging;
using ProcessingDataLayer;
using ReactiveUI;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PracticaDiagnostico.ViewModels
{
	public class PageIgualesViewModel : ViewModelBase
	{
    #region Fields
    bool convertEnabled;
    Bitmap imageA;
    Bitmap imageB;
    #endregion

    #region Properties

    public bool ConvertEnabled
    {
      get
      {
        return convertEnabled;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref convertEnabled, value);
        convertEnabled = value;
      }
    }

    public Bitmap ImageA
    {
      get
      {
        return imageA;
      }
      set
      {
        ConvertEnabled = value != null && ImageB != null;
        this.RaiseAndSetIfChanged(ref imageA, value);
        imageA = value;
      }
    }

    public Bitmap ImageB
    {
      get
      {
        return imageB;
      }
      set
      {
        ConvertEnabled = ImageA != null && value != null;
        this.RaiseAndSetIfChanged(ref imageB, value);
        imageB = value;
      }
    }
    public IMathService Service { get; set; }
    #endregion

    #region Constructor
    public PageIgualesViewModel()
    {
      Service = new MathService();
    }
    #endregion

    #region Methods

    public void OnCalculateClick()
    {
      try
      {
        string message = string.Empty;

        if (ImageA.Dpi != ImageB.Dpi)
          message += "La imagen posee diferente resolución \n";

        if (ImageA.Size != ImageB.Size)
          message += "La imagen posee diferente tamaño \n";

        /*
        if (!ImageA.Equals(ImageB))
          message += "Las imagen son diferentes";
        */

        if (string.IsNullOrEmpty(message))
          message = "Las Imagenes son iguales";

        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Info", message);
        messageBoxStandardWindow.Show();
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
    }

    public async Task OnLoadClick(string target)
    {
      try
      {
        var path = await GetPath();
        if (!string.IsNullOrEmpty(path))
        {
          if (target == "ImageA")
            ImageA = new Bitmap((string)path);
          else if (target == "ImageB")
            ImageB = new Bitmap((string)path);
        }
          
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        await messageBoxStandardWindow.Show();
      }
    }


    public async Task OnLoadImageBClick()
    {
      try
      {
        var path = await GetPath();
        if (!string.IsNullOrEmpty(path))
          ImageB = new Bitmap((string)path);
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        await messageBoxStandardWindow.Show();
      }
    }

    public async Task<string> GetPath()
    {
      var dialog = new OpenFileDialog();
      dialog.Filters.Add(new FileDialogFilter() { Name = "Text", Extensions = { "jpg", "png", "bmp" } });
      var result = await dialog.ShowAsync(new Window());

      return result.FirstOrDefault();
    }
    #endregion
  }
}