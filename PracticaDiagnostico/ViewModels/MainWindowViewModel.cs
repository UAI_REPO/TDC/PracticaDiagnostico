﻿using PracticaDiagnostico.Models.Enum;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Text;

namespace PracticaDiagnostico.ViewModels
{
	public class MainWindowViewModel : ReactiveObject //ViewModelBase
  {
    #region Fields
    ViewModelBase content;
    string greeting;
    bool menuEnabled;
    #endregion

    #region Properties
    public ReactiveCommand<string, Unit> OnMenuClick { get; }

    public string Greeting
    {
      get => "Pool de Ejemplos";
      private set => this.RaiseAndSetIfChanged(ref greeting, value);
    }

    public bool MenuEnabled
    {
      get
      {
        return menuEnabled;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref menuEnabled, value);
        menuEnabled = value;
      }
    }

    public ViewModelBase Content
    {
      get => content;
      private set => this.RaiseAndSetIfChanged(ref content, value);
    }
    #endregion

    #region Constructor
    public MainWindowViewModel()
    {
      OnMenuClick = ReactiveCommand.Create<string>(MenuClick);
      MenuEnabled = true;
      Content = new PageHomeViewModel();
    }
    #endregion

    #region Methods

    void MenuClick(string parameter)
    {
      MenuItems item;
      if (!Enum.TryParse(parameter, out item))
        return;

      switch (item)
      {
        case MenuItems.MenuItem_NumerosPares:
          Content = new PageNumerosParesViewModel();
          break;
        case MenuItems.MenuItem_Ordenamiento:
          Content = new PageOrdenamientoViewModel();
          break;
        case MenuItems.MenuItem_Calendario:
          Content = new PageCalendarioViewModel();
          break;
        case MenuItems.MenuItem_Serie:
          Content = new PageSerieViewModel();
          break;
        case MenuItems.MenuItem_Iguales:
          Content = new PageIgualesViewModel();
          break;
        case MenuItems.MenuItem_Ohm:
          Content = new PageOhmViewModel();
          break;
        case MenuItems.MenuItem_Distancia:
          Content = new PageDistanciaViewModel();
          break;
        case MenuItems.MenuItem_Hash:
          Content = new PageHashViewModel();
          break;
        case MenuItems.MenuItem_Matriz:
          Content = new PageMatrizEscalonadaViewModel();
          break;
        case MenuItems.MenuItem_Parabolico:
          Content = new PageTiroParabolicoViewModel();
          break;
        default:
          break;
      }
    }

    public void ChangeMenuVisibility()
    {
      MenuEnabled = !MenuEnabled;
    }

    #endregion

  }
}
