﻿using ProcessingDataLayer;
using ReactiveUI;
using System;
using System.Linq;

namespace PracticaDiagnostico.ViewModels
{
	public class PageMatrizEscalonadaViewModel : ViewModelBase
	{
    #region Fields
    string[][] matrix;
    string result;
    string searchNumber;
    #endregion

    #region Properties

    public string[][] Matrix
    {
      get
      {
        if(matrix == null)
          matrix = new string[][]
          {
              new string[] {"0","0","0","0","0"},
              new string[] {"0","0","0","0"},
              new string[] {"0","0"}
          };

        return matrix;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref matrix, value);
        matrix = value;
      }
    }

    public string Result
    {
      get
      {
        return result;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref result, value);
        result = value;
      }
    }

    public string SearchNumber
    {
      get
      {
        return searchNumber;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref searchNumber, value);
        searchNumber = value;
      }
    }
    
    public IMathService Service { get; set; }
    #endregion

    #region Constructor
    public PageMatrizEscalonadaViewModel()
    {
      Service = new MathService();

    }
    #endregion

    #region Methods
    public void OnCalculateClick()
    {
      try
      {
        
        var matrix = GetMatrix();

        float number = 0;
        if (!float.TryParse(SearchNumber, out number))
          throw new Exception($"El numero de busqueda es invalido. Valor [{SearchNumber}]");
        
        var coordinates = Service.GetNumbersFromMatrix(matrix, number);

        Result = string.Join(",", coordinates.Select(x => $"({x.Item1 + 1},{x.Item2 + 1})"));
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
    }

    public void OnDefaultsClick()
    {
      try
      {
        var matrix = new float[][]
        {
          new float[] {1,3,5,7,9},
          new float[] {0,2,4,6},
          new float[] {11,22}
        };

        LoadMatrix(matrix);

        SearchNumber = "4";
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
    }

    private void LoadMatrix(float[][] matrix)
    {
      for (int i = 0; i < matrix.Length; i++)
      {
        Matrix[i] = new string[matrix[i].Length];
        for (int j = 0; j < matrix[i].Length; j++)
        {
          Matrix[i][j] = matrix[i][j].ToString();
        }
      }

      Matrix = Matrix.ToArray();
    }

    private float[][] GetMatrix()
    {
      var matrix = new float[Matrix.Length][];

      for (int i = 0; i < Matrix.Length; i++)
      {
        matrix[i] = new float[Matrix[i].Length];
        for (int j = 0; j < matrix[i].Length; j++)
        {
          float cell = 0;
          if (!float.TryParse(Matrix[i][j], out cell))
            throw new Exception($"No se puede convertir el numero de la celda [{i},{j}] Con valor: {Matrix[i][j]}");

          matrix[i][j] = cell;
        }
      }

      return matrix;
    }

    #endregion
  }
}