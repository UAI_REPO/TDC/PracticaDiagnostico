﻿using ProcessingDataLayer;
using ReactiveUI;
using System;

namespace PracticaDiagnostico.ViewModels
{
  public class PageTiroParabolicoViewModel : ViewModelBase
  {
    #region Fields
    bool calculateCommandEnable;
    string initialSpeed;
    string shotAngle;
    string verticalDistance;
    string horizontalDistance;

    #endregion

    #region Properties

    public string InitialSpeed
    {
      get
      {
        return initialSpeed;
      }
      set
      {
        float realValue;
        CalculateCommandEnable = float.TryParse(value, out realValue) && float.TryParse(ShotAngle, out realValue);
        this.RaiseAndSetIfChanged(ref initialSpeed, value);
        initialSpeed = value;
      }
    }

    public string ShotAngle
    {
      get
      {
        return shotAngle;
      }
      set
      {
        float realValue;
        CalculateCommandEnable = float.TryParse(InitialSpeed, out realValue) && float.TryParse(value, out realValue);
        this.RaiseAndSetIfChanged(ref shotAngle, value);
        shotAngle = value;
      }
    }

    public string VerticalDistance
    {
      get
      {
        return verticalDistance;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref verticalDistance, value);
        verticalDistance = value;
      }
    }

    public string HorizontalDistance
    {
      get
      {
        return horizontalDistance;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref horizontalDistance, value);
        horizontalDistance = value;
      }
    }

    public bool CalculateCommandEnable
    {
      get
      {
        return calculateCommandEnable;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref calculateCommandEnable, value);
        calculateCommandEnable = value;
      }
    }
    
    public IPhysicService Service { get; set; }
    #endregion

    #region Constructor
    public PageTiroParabolicoViewModel()
    {
      Service = new PhysicService();

    }
    #endregion

    #region Methods
    public void OnCalculateClick()
    {
      try
      {
        float speed;
        float angle;

        float.TryParse(InitialSpeed, out speed);
        float.TryParse(ShotAngle, out angle);
        Tuple<double, double> result = Service.CalculateParabolicShot(speed, angle);
        HorizontalDistance = $"{result.Item1:N2}";
        VerticalDistance = $"{result.Item2:N2}";
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
    }

    #endregion
  }
}