﻿using Avalonia;
using Avalonia.Controls;
using ProcessingDataLayer;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PracticaDiagnostico.ViewModels
{
	public class PageOrdenamientoViewModel : ViewModelBase
	{
    #region Fields
    string inputNumber;
    bool inputEnabled;
    bool showResult;
    IList<int> listBoxItems;
    IList<int> sortedList;
    int exchanges;
    #endregion

    #region Properties

    public bool InputEnabled
    {
      get
      {
        return inputEnabled;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref inputEnabled, value);
        inputEnabled = value;
      }
    }

    public bool ShowResult
    {
      get
      {
        return showResult;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref showResult, value);
        showResult = value;
      }
    }

    public string InputNumber
    {
      get
      {
        return inputNumber;
      }
      set
      {
        ValidateNumber(value);
        this.RaiseAndSetIfChanged(ref inputNumber, value);
        inputNumber = value;
      }
    }

    public IList<int> ListBoxItems
    {
      get
      {
        return listBoxItems;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref listBoxItems, value);
        listBoxItems = value;
      }
    }

    public IList<int> SortedList
    {
      get
      {
        return sortedList;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref sortedList, value);
        sortedList = value;
      }
    }

    public int Exchanges
    {
      get
      {
        return exchanges;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref exchanges, value);
        exchanges = value;
      }
    }

    public IMathService Service { get; set; }
    #endregion

    #region Constructor
    public PageOrdenamientoViewModel()
    {
      InputEnabled = false;
      Service = new MathService();
    }
    #endregion

    #region Methods

    private void ValidateNumber(string value)
    {
      int realValue = 0;
      if (int.TryParse(value, out realValue))
        InputEnabled = true;
      else
        InputEnabled = false;
    }

    public void OnCalculateClick()
    {
      try
      {
        var result = Service.BubbleSort(listBoxItems);
        SortedList = result.Key;
        Exchanges = result.Value;

        ShowResult = true;
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
    }

    public void OnAddItemClick()
    {

      int realValue = 0;
      int.TryParse(InputNumber, out realValue);

      ListBoxItems.Add(realValue);
      ListBoxItems = ListBoxItems.ToList(); //Force to call OnChange

      InputNumber = string.Empty;
    }

    public void OnClearItemsClick()
    {
      ShowResult = false;
      ListBoxItems = new List<int>();
    }

    public void OnLoadDefaultItemsClick()
    {
      ListBoxItems = new List<int>() { 23, 1, 5, 2, 98, 14, 76, 98, 104, 3, 9 };
    }
    #endregion
  }
}