﻿using ProcessingDataLayer;
using ReactiveUI;
using System;

namespace PracticaDiagnostico.ViewModels
{
	public class PageHashViewModel : ViewModelBase
	{
    #region Fields
    string message;
    bool enableCalculateButton;
    string result;
    #endregion

    #region Properties

    public string Message
    {
      get
      {
        return message;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref message, value);
        message = value;

        if (!string.IsNullOrEmpty(value))
          EnableCalculateButton = true;
        else
          EnableCalculateButton = false;
      }
    }

    public bool EnableCalculateButton
    {
      get
      {
        return enableCalculateButton;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref enableCalculateButton, value);
        enableCalculateButton = value;
      }
    }

    public string Result
    {
      get
      {
        return result;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref result, value);
        result = value;
      }
    }

    public IMathService Service { get; set; }
    #endregion

    #region Constructor
    public PageHashViewModel()
    {
      Service = new MathService();
    }
    #endregion

    #region Methods
    public void OnCalculateClick()
    {
      try
      {
        Result = Service.GenerateHash(Message);
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
    }

    #endregion
  }
}