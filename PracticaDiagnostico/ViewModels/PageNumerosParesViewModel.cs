﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Text;
using ProcessingDataLayer;

namespace PracticaDiagnostico.ViewModels
{
	class PageNumerosParesViewModel : ViewModelBase
  {
    #region Fields
    string inputNumber;
    bool inputEnabled;
    bool showResult;
    IList<int> listBoxItems;
    IList<int> evenNumber;
    #endregion

    #region Properties

    public bool InputEnabled
    {
      get
      {
        return inputEnabled;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref inputEnabled, value);
        inputEnabled = value;
      }
    }

    public bool ShowResult
    {
      get
      {
        return showResult;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref showResult, value);
        showResult = value;
      }
    }

    public string InputNumber
    {
      get
      {
        return inputNumber;
      }
      set
      {
        ValidateNumber(value);
        this.RaiseAndSetIfChanged(ref inputNumber, value);
        inputNumber = value;
      }
    }

    public IList<int> ListBoxItems
    {
      get
      {
        return listBoxItems;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref listBoxItems, value);
        listBoxItems = value;
      }
    }

    public IList<int> EvenNumber
    {
      get
      {
        return evenNumber;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref evenNumber, value);
        evenNumber = value;
      }
    }

    public int CountEvenNumber
    {
      get
      {
        return evenNumber.Count;
      }
    }

    public IMathService Service { get; set; }
    #endregion

    #region Constructor
    public PageNumerosParesViewModel()
    {
      InputEnabled = false;
      Service = new MathService();
    }
    #endregion

    #region Methods

    private void ValidateNumber(string value)
    {
      int realValue = 0;
      if (int.TryParse(value, out realValue))
        InputEnabled = true;
      else
        InputEnabled = false;
    }

    public void OnCalculateClick()
    {
      try
      {
        EvenNumber = Service.FindEvenNumbers(listBoxItems);
        ShowResult = true;
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
    }

    public void OnAddItemClick()
    {

      int realValue = 0;
      int.TryParse(InputNumber, out realValue);

      ListBoxItems.Add(realValue);
      ListBoxItems = ListBoxItems.ToList();

      InputNumber = string.Empty;
    }

    public void OnClearItemsClick()
    {
      ShowResult = false;
      ListBoxItems = new List<int>();
    }

    public void OnLoadDefaultItemsClick()
    {
      ListBoxItems = new List<int>() { 45, 23, 52, 68, 2, 98, 106, 107, 465, 23, 18 };
    }
    #endregion
  }
}
