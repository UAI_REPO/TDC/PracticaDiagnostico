﻿using Avalonia.Media.Imaging;
using ProcessingDataLayer;
using ReactiveUI;
using System;

namespace PracticaDiagnostico.ViewModels
{
	public class PageOhmViewModel : ViewModelBase
	{
		#region Fields
		string current;
		string voltage;
		string resistance;
		bool currentEnable;
		bool voltageEnable;
		bool resistanceEnable;
		#endregion

		#region Properties

		public bool ResistanceEnable
		{
			get
			{
				return resistanceEnable;
			}
			set
			{

				this.RaiseAndSetIfChanged(ref resistanceEnable, value);
				resistanceEnable = value;
			}
		}

		public bool VoltageEnable
		{
			get
			{
				return voltageEnable;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref voltageEnable, value);
				voltageEnable = value;
			}
		}

		public bool CurrentEnable
		{
			get
			{
				return currentEnable;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref currentEnable, value);
				currentEnable = value;
			}
		}

		public string Resistance
		{
			get
			{
				return resistance;
			}
			set
			{
				if (value == null)
				{
					this.RaiseAndSetIfChanged(ref resistance, value);
					resistance = value;
					return;
				}

				float realValue;
				if (!float.TryParse(value, out realValue))
					return;

				if (ResistanceEnable)
				{
					if (!string.IsNullOrEmpty(Current))
						CalculateVoltage(value, Current);
					else if (!string.IsNullOrEmpty(Voltage))
						CalculateCurrent(value, Voltage);
				}

				this.RaiseAndSetIfChanged(ref resistance, value);
				resistance = value;
			}
		}

		public string Voltage
		{
			get
			{
				return voltage;
			}
			set
			{
				if (value == null)
				{
					this.RaiseAndSetIfChanged(ref voltage, value);
					voltage = value;
					return;
				}

				float realValue;
				if (!float.TryParse(value, out realValue))
					return;

				if (VoltageEnable)
				{
					if (!string.IsNullOrEmpty(Current))
						CalculateResistance(Current, value);
					else if (!string.IsNullOrEmpty(Resistance))
						CalculateCurrent(Resistance, value);
				}
				
				this.RaiseAndSetIfChanged(ref voltage, value);
				voltage = value;

			}
		}

		public string Current
		{
			get
			{
				return current;
			}
			set
			{
				if (value == null)
				{
					this.RaiseAndSetIfChanged(ref current, value);
					current = value;
					return;
				}

				float realValue;

				if (!float.TryParse(value, out realValue))
					return;

				if (CurrentEnable)
				{
					if (!string.IsNullOrEmpty(Voltage))
						CalculateResistance(value, Voltage);
					else if (!string.IsNullOrEmpty(Resistance))
						CalculateVoltage(Resistance, value);
				}
				
				this.RaiseAndSetIfChanged(ref current, value);
				current = value;

			}
		}

		public Bitmap ParabolicImage
		{
			get
			{
				return new Bitmap("\\Content\\Images\\physics-of-football-kick.gif");
			}
			
		}

		public IPhysicService Service { get; set; }
		#endregion

		#region Constructor
		public PageOhmViewModel()
		{
			Service = new PhysicService();
			CurrentEnable = true;
			VoltageEnable = true;
			ResistanceEnable = true;
		}
		#endregion

		#region Methods


		public void CalculateVoltage(string raw_resistance, string raw_current)
		{
			try
			{
				VoltageEnable = false;
				float resistanceValue;
				float.TryParse(raw_resistance, out resistanceValue);

				float currentValue;
				float.TryParse(raw_current, out currentValue);

				Voltage = Service.CalculateVoltage(resistanceValue, currentValue).ToString();
			}
			catch (Exception e)
			{
				var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
				messageBoxStandardWindow.Show();
			}
		}

		public void CalculateCurrent(string raw_resistance, string raw_voltage)
		{
			try
			{
				CurrentEnable = false;
				float voltageValue;
				float.TryParse(raw_voltage, out voltageValue);

				float resistanceValue;
				float.TryParse(raw_resistance, out resistanceValue);

				if (resistanceValue == 0)
					return;

				Current = Service.CalculateCurrent(resistanceValue, voltageValue).ToString();
			}
			catch (Exception e)
			{
				var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
				messageBoxStandardWindow.Show();
			}
		}

		public void CalculateResistance(string raw_current, string raw_voltage)
		{
			try
			{
				ResistanceEnable = false;
				float voltageValue;
				float.TryParse(raw_voltage, out voltageValue);

				float currentValue;
				float.TryParse(raw_current, out currentValue);

				if (currentValue == 0)
					return;

				Resistance = Service.CalculateResistance(currentValue, voltageValue).ToString();
			}
			catch (Exception e)
			{
				var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
				messageBoxStandardWindow.Show();
			}
		}

		public void OnClearClick()
		{
			try
			{
				Current = null;
				Voltage = null;
				Resistance = null;
				ResistanceEnable = true;
				VoltageEnable = true;
				CurrentEnable = true;
			}
			catch (Exception e)
			{
				var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
				messageBoxStandardWindow.Show();
			}
		}

		#endregion
	}
}