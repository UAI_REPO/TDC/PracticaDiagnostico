﻿using ProcessingDataLayer;
using ReactiveUI;
using System;
using System.Collections.Generic;

namespace PracticaDiagnostico.ViewModels
{
	public class PageSerieViewModel : ViewModelBase
	{
    #region Fields
    string inputNumber;
    bool inputEnabled;
    string serieResult;
    #endregion

    #region Properties

    public bool InputEnabled
    {
      get
      {
        return inputEnabled;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref inputEnabled, value);
        inputEnabled = value;
      }
    }

    public string InputNumber
    {
      get
      {
        return inputNumber;
      }
      set
      {
        ValidateNumber(value);
        this.RaiseAndSetIfChanged(ref inputNumber, value);
        inputNumber = value;
      }
    }

    public string SerieResult
    {
      get
      {
        return serieResult;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref serieResult, value);
        serieResult = value;
      }
    }
    public IMathService Service { get; set; }
    #endregion

    #region Constructor
    public PageSerieViewModel()
    {
      InputEnabled = false;
      Service = new MathService();
    }
    #endregion

    #region Methods

    private void ValidateNumber(string value)
    {
      int realValue = 0;
      if (int.TryParse(value, out realValue))
        InputEnabled = true;
      else
        InputEnabled = false;
    }

    public void OnCalculateClick()
    {
      try
      {
        int realValue = 0;
        int.TryParse(InputNumber, out realValue);

        var list = Service.GetFibonacciSerie(realValue);
        SerieResult = string.Join(',',list);
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
    }

    #endregion
  }
}