﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PracticaDiagnostico.Views
{
	public class PageIgualesView : UserControl
	{
		public PageIgualesView()
		{
			this.InitializeComponent();
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}

  }
}
