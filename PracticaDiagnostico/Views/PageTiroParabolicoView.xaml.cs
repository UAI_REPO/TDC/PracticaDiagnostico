﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace PracticaDiagnostico.Views
{
	public class PageTiroParabolicoView : UserControl
	{
		public PageTiroParabolicoView()
		{
			this.InitializeComponent();
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
