﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace PracticaDiagnostico.Views
{
	public class PageOrdenamientoView : UserControl
	{
		public PageOrdenamientoView()
		{
			this.InitializeComponent();
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
