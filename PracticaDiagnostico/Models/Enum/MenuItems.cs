﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracticaDiagnostico.Models.Enum
{
	public enum MenuItems
	{
		MenuItem_NumerosPares,
		MenuItem_Ordenamiento,
		MenuItem_Calendario,
		MenuItem_Serie,
		MenuItem_Iguales,
		MenuItem_Ohm,
		MenuItem_Distancia,
		MenuItem_Hash,
		MenuItem_Matriz,
		MenuItem_Parabolico
	}
}
